<?php

if(isset($_GET["dep"]) && $_GET["dep"]!= ""){
    $url = "https://vitemadose.gitlab.io/vitemadose/" . $_GET["dep"] . ".json";
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    $data = curl_exec($curl);
    curl_close($curl);
    header('Content-Type: application/json;charset=utf-8');
    header('Access-Control-Allow-Origin: *');
    echo(json_encode(json_decode($data, true)));
}else if(isset($_GET["doctolib"]) && $_GET["doctolib"]!= ""){
    $response = [];
    $booking_url = "https://partners.doctolib.fr/booking/" . $_GET["doctolib"] . ".json";
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $booking_url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    $res = curl_exec($curl);
    $data = json_decode($res, true);
    $response["metadata"] = $data["data"];
    $visit_motive_ids = "";
    $visit_motive_names = "";
    foreach($data["data"]["visit_motives"] as $v){
        if(strpos($v["name"], "1ère injection vaccin COVID-19")===0 || strpos($v["name"], "1re injection vaccin COVID-19")===0){
            $visit_motive_ids .= $v["id"]."-";
            $visit_motive_names .= $v["name"]." + ";
        }
    }
    $visit_motive_ids = substr($visit_motive_ids, 0, -1);
    $visit_motive_names = substr($visit_motive_names, 0, -3);
    $agenda_ids = "";
    foreach($data["data"]["agendas"] as $a){
        $agenda_ids .= $a["id"]."-";
    }
    $agenda_ids = substr($agenda_ids, 0, -1);

    curl_close($curl);
    
    $url = "https://pro.doctolib.fr/availabilities.json?start_date=".date("Y-m-d")."&visit_motive_ids=".$visit_motive_ids."&agenda_ids=".$agenda_ids."&practice_ids=".$data["data"]["places"][0]["practice_ids"][0];
    $response["request"] = [
        "start_date" => date("Y-m-d"),
        "visit_motive_ids" => $visit_motive_ids,
        "visit_motive_names" => $visit_motive_names,
        "agenda_ids" => $agenda_ids,
        "practice_ids" => $data["data"]["places"][0]["practice_ids"][0],
        "availabilities_url" => $url,
        "booking_url" => $booking_url,
        "request_url" => "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']
    ];
    
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    $res = curl_exec($curl);
    $data = json_decode($res, true);
    if($data){
        $response["availabilities"] = $data;
    }else{
        $response["availabilities"] = $res;
    }
    curl_close($curl);
    
    $response = json_encode($response);
    header('Content-Type: application/json;charset=utf-8');
    header('Access-Control-Allow-Origin: *');
    echo($response);
}

?>