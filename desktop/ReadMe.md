# Application d'ordinateur (Windows/Mac/Linux)

## Windows

Télécharger pour Windows : [.exe](https://framagit.org/DavidLibeau/notification-vaccin/-/raw/main/desktop/dist/Notification%20Vaccin%20Covid19%20Setup%201.0.1.exe)

## Mac

Télécharger pour Mac : [.dmg](https://framagit.org/DavidLibeau/notification-vaccin/-/raw/main/desktop/dist/Notification%20Vaccin%20Covid19-1.0.1.dmg), [.app](https://framagit.org/DavidLibeau/notification-vaccin/-/raw/main/desktop/dist/Notification%20Vaccin%20Covid19-1.0.1-mac.zip) 

## Linux

Télécharger pour Linux : [.snap](https://framagit.org/DavidLibeau/notification-vaccin/-/raw/main/desktop/dist/notification-vaccin_1.0.1_amd64.snap)