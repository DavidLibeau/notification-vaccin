window.addEventListener('DOMContentLoaded', () => {
    console.log("Page loaded");

    let $ = jQuery = require('jquery');
    const {
        ipcRenderer
    } = require('electron');
    // Global var
    var map, markers, iconDefault, iconActive, notifInterval, realtimeDoctolibInterval;

    function activateNotification() {
        if (!("Notification" in window)) {
            alert("Ce navigateur web ne supporte pas les notification. L'application ne va pas fonctionner.");
        } else if (Notification.permission === "granted") {
            notifGranted();
        } else if (Notification.permission !== "denied") {
            Notification.requestPermission().then(function (permission) {
                if (permission === "granted") {
                    notifGranted();
                }
            });
        }
    }

    function notifGranted() {
        var notification = new Notification("Vous êtes abonné aux notification !");
        $("#notification").append("<button id=\"pauseNotif\"><i class=\"fa fa-pause\" aria-hidden=\"true\"></i><span class=\"hideMobile\"> Pause</span></button>");
        $("#pauseNotif").click(function () {
            $("#pauseNotif").remove();
            clearInterval(notifInterval);
            $("#notification").append("<button id=\"pauseNotif\"><i class=\"fa fa-play\" aria-hidden=\"true\"></i><span class=\"hideMobile\"> Activer</span></button>");
            $("#pauseNotif").click(function () {
                $("#pauseNotif").remove();
                notifGranted();
            });
        });
        $("#notification>h2").html("<i class=\"fa fa-bell-o\" aria-hidden=\"true\"></i> Notifications activées");
        $("#notification>p:first-of-type").html("N'hésitez pas à ajouter d'autres centres de vaccination en favoris pour avoir plus de notifications.");
        $("#notification>p:nth-of-type(2)").html("");

        checkCenterAvailabilityFromSave(generateJsonSaveFromHTML(), true);
        notifInterval = setInterval(function () {
            checkCenterAvailabilityFromSave(generateJsonSaveFromHTML(), true);
        }, 5 * 60 * 1000);
    }
    $("#activateNotification").click(activateNotification);

    function doctolibRealtime() {
        console.log("doctolibRealtime");
        var blockedByDoctolib = false;
        $("#fav [data-plateforme=\"Doctolib\"]").each(async function () {
            var center = $(this).data("json");
            if(!center){
                center = {};
            }
            console.log(center);
            var result;
            try {
                result = await $.ajax({
                    url: "https://dav.li/notification-vaccin/api.php?doctolib=" + $(this).attr("data-link")
                });
                console.log(result);
                var availabilities;
                if (result.availabilities.indexOf("error") == -1) {
                    availabilities = result.availabilities;
                } else {
                    var result = await $.ajax({
                        url: result.request.availabilities_url
                    });
                    if (result.availabilities.indexOf("error") == -1) {
                        availabilities = result.availabilities;
                    }
                }
            } catch (e) {
                blockedByDoctolib = true;
                console.error("Ajax error for doctolib=" + $(this).attr("data-link"));
            }
            if (availabilities) {
                console.log(availabilities);
                var slots0 = [];
                if (availabilities[0].slots) {
                    slots0 = availabilities[0].slots;
                }
                var slots1 = [];
                if (availabilities[1].slots) {
                    slots1 = availabilities[1].slots;
                }
                var slotsCount = slots0.length + slots1.length;
                console.log(slotsCount);
                if (slotsCount > 0) {
                    center["appointment_schedules"] = [{
                        "name": "1_days",
                        "total": slots0.length
                        }, {
                        "name": "2_days",
                        "total": slots1.length
                        }];
                    $(this).remove();
                    appendCenter(center, "#fav", "<button><i class=\"fa fa-trash\" aria-hidden=\"true\"></i> <span class=\"hideMobile\">Supprimer ce favori</span></button>");
                    $("[data-id=\""+center.internal_id+"\"] button").click(deleteCenterParent);
                    sendNotification(center);
                }
            }


        }).promise().done(function () {
            if (blockedByDoctolib) {
                alert("Malheureusement, Doctolib ne vous autorise plus à accéder à ses données. Désactivez la mise à jour via Doctolib dans les paramètres.");
            }
        });
    }

    function saveFav() {
        console.log("Save Fav");
        //window.location.href = window.location.href.split("#")[0] + "#" + generateInlineSaveFromHTML();
        ipcRenderer.send('saveFav', generateJsonSaveFromHTML());
    }
    $("#saveFav").click(saveFav);

    async function checkCenterAvailabilityFromSave(save, notify) {
        for (var s in save) {
            $("#refresh .fa-refresh").addClass("fa-spin");
            var depData = await getDepData(s);
            $("#refresh .fa-refresh").removeClass("fa-spin");
            if (depData) {
                for (var t in save[s]) {
                    for (var d in depData) {
                        var center = depData[d];
                        if (center.internal_id == save[s][t]) {
                            var date = new Date();
                            $("#refresh>span").text("Dernière mise à jour faite à " + date.getHours() + ":" + date.getMinutes());
                            document.title = "🔄[" + date.getHours() + ":" + date.getMinutes() + "] – Notification vaccin Covid19";
                            if (center.plateforme != 'Doctolib' || !doctolibRealtime) {
                                $("#fav [data-id=\"" + center.internal_id + "\"]").replaceWith(buildHTMLCenter(center, "li", "<button><i class=\"fa fa-trash\" aria-hidden=\"true\"></i> <span class=\"hideMobile\">Supprimer ce favori</span></button>"));
                                var hasAvailability = false;
                                for (var a in center.appointment_schedules) {
                                    if (center.appointment_schedules && (center.appointment_schedules[a].name == "1_days" || center.appointment_schedules[a].name == "2_days") && center.appointment_schedules[a].total != 0) {
                                        hasAvailability = true;
                                    }
                                }
                                if (notify && hasAvailability) {
                                    sendNotification(center);
                                }
                            }

                        }
                    }
                }
            }
        }
    }
    $("#refresh").click(function () {
        checkCenterAvailabilityFromSave(generateJsonSaveFromHTML(), false);
    });

    function generateJsonSaveFromHTML() {
        var save = {};
        $("#fav>li").each(function (index, value) {
            if (!save[$(this).data("dep")]) {
                save[$(this).data("dep")] = [];
            }
            save[$(this).data("dep")].push($(this).attr("data-id"));
        });
        return save;
    }

    function generateInlineSaveFromHTML() {
        var save = generateJsonSaveFromHTML();
        return transformJsonToInlineSave(save);
    }

    function transformJsonToInlineSave(save) {
        var inline = "";
        for (var s in save) {
            inline += s + "-";
            for (var t in save[s]) {
                inline += save[s][t] + "-";
            }
            inline += "-";
        }
        inline = inline.slice(0, inline.length - 2);
        return inline;
    }

    function transformInlineToJsonSave(inline) {
        var save = {};
        var inlineDep = inline.split("--");
        for (var d in inlineDep) {
            if (inlineDep[d] != "") {
                var inlineLocal = inlineDep[d].split("-");
                var dep = inlineLocal[0];
                if (!save[dep]) {
                    save[dep] = [];
                }
                for (var l in inlineLocal) {
                    if (l != 0) {
                        save[dep].push(inlineLocal[l]);
                    }
                }
            }
        }
        return save;
    }

    async function restoreJsonSaveToHTML(save, callback) {
        $("#fav").html("");
        $("#refresh").show();
        $("#saveFav").show();
        for (var s in save) {
            var depData = await getDepData(s);
            if (depData) {
                for (var t in save[s]) {
                    for (var d in depData) {
                        var center = depData[d];
                        if (center.internal_id == save[s][t]) {
                            appendCenter(center, "#fav", "<button><i class=\"fa fa-trash\" aria-hidden=\"true\"></i> <span class=\"hideMobile\">Supprimer ce favori</span></button>");
                        }
                    }
                }
            }
        }
        $("#fav>li button").click(deleteCenterParent);
        callback();
    }

    function appendCenter(center, where, footer) {
        $(where).append($(buildHTMLCenter(center, "li", footer)).data("json", center));
    }

    function buildHTMLCenter(center, mainEl, footer) {
        var vaccine_type = "";
        for (var v in center.vaccine_type) {
            vaccine_type += "<li>" + center.vaccine_type[v] + "</li>";
        }
        var tomorrow = 0;
        for (var a in center.appointment_schedules) {
            if (center.appointment_schedules && (center.appointment_schedules[a].name == "1_days" || center.appointment_schedules[a].name == "2_days")) {
                tomorrow += center.appointment_schedules[a].total;
            }
        }
        var directUpdateParams = "";
        if (center.plateforme == 'Doctolib') {
            directUpdateParams += "data-plateforme=\"Doctolib\" data-link=\"" + center.url.split('/')[center.url.split('/').length - 1] + "\"";
        }
        return "<" + mainEl + " data-id=\"" + center.internal_id + "\" " + directUpdateParams + " class=\"center " + (tomorrow > 0 ? "active" : "") + "\" data-dep=\"" + center.departement + "\"><h3>" + center.nom + "</h3>" + (tomorrow > 0 ? "<span class=\"notif\" aria-hidden=\"true\">" + tomorrow + "</span>" : "") + "<ul><li>Adresse : " + center.metadata.address + "</li><li>Vaccin(s) : <ul>" + vaccine_type + "</ul><li>Plateforme : " + center.plateforme + "</li><li>Rendez-vous dans les 24h : " + tomorrow + "</li></ul><p><a class=\"btn\" href=\"" + center.url + "\" target=\"_blank\" rel=\"noreferrer noopener\">Prendre rendez-vous <i class=\"fa fa-external-link\" aria-hidden=\"true\"></i></a> " + footer + "</p></" + mainEl + ">";
    }

    $("main form").submit(async function (e) {
        e.preventDefault();
        $("[type=\"submit\"]").after("<i style=\"margin-left:10px\" id=\"tempRefreshIcon\" class=\"fa fa-refresh fa-spin\"></i>");
        if (!$("#search").val()) {
            $("#search").val("75");
        }
        var centers = await getDepData($("#search").val());
        console.log(centers);
        if (centers.length > 0) {
            $("#searchResults").html("");
            for (var c in centers) {
                if (centers[c].url) {
                    if ($("#fav [data-id=\"" + centers[c].internal_id + "\"]").length == 0) {
                        appendCenter(centers[c], "#searchResults", "<button><i class=\"fa fa-star\" aria-hidden=\"true\"></i> Ajouter aux favoris</button>");

                    }
                }
            }
            if ($("#map").length == 0) {
                console.log("Adding map el");
                $("#searchResults").before("<div id=\"map\"><button><i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i> Afficher une carte des résultats</button></div>");
                $("#map>button").click(function () {
                    $("head").append('<link rel="stylesheet" href="https://dav.li/leaflet/leaflet.css"/>');
                    $.getScript("https://dav.li/leaflet/leaflet.js", function () {
                        $("#map").html("").height("400px");
                        map = L.map('map').setView([46.777, 2.659], 6);
                        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        }).addTo(map);
                        iconDefault = L.icon({
                            iconUrl: 'marker-default.png',
                            iconSize: [24, 35],
                            iconAnchor: [24, 35],
                            popupAnchor: [-12, -35]
                        });
                        iconActive = L.icon({
                            iconUrl: 'marker-active.png',
                            iconSize: [24, 35],
                            iconAnchor: [24, 35],
                            popupAnchor: [-12, -35]
                        });
                        markers = L.layerGroup();
                        map.addLayer(markers);
                        map.on("popupopen", function (e) {
                            var marker = e.popup._source;
                            $("#map .center button").click(onFavClick).click(function () {
                                markers.removeLayer(marker);
                            });
                        });
                        $("form").submit();
                    });
                });
            } else if (markers) {
                markers.clearLayers();
                var lat = [];
                var lon = [];
                for (var c in centers) {
                    var center = centers[c];
                    if (center.url) {
                        if ($("#fav [data-id=\"" + center.internal_id + "\"]").length == 0) {
                            lat.push(center.location.latitude);
                            lon.push(center.location.longitude);
                            var options = {
                                icon: iconDefault
                            };
                            var hasAvailability = false;
                            for (var a in center.appointment_schedules) {
                                if (center.appointment_schedules && (center.appointment_schedules[a].name == "1_days" || center.appointment_schedules[a].name == "2_days") && center.appointment_schedules[a].total != 0) {
                                    hasAvailability = true;
                                }
                            }
                            if (hasAvailability) {
                                options.icon = iconActive;
                            }
                            var marker = L.marker([center.location.latitude, center.location.longitude], options).bindPopup(buildHTMLCenter(center, "div", "<button><i class=\"fa fa-star\" aria-hidden=\"true\"></i> Ajouter aux favoris</button>"));
                            markers.addLayer(marker);
                        }
                    }
                }
                map.flyTo([lat.reduce(function (a, b) {
                    return a + b;
                }) / lat.length, lon.reduce(function (a, b) {
                    return a + b;
                }) / lon.length], 9);
            }
            $("#searchResults>.center button").click(onFavClick);
        } else {
            $("#searchResults").html("<li>Aucun résultat.</li>");
        }
        $("#tempRefreshIcon").remove();
    });

    function onFavClick() {
        console.log("fav");
        if (!$(this).parents(".center").hasClass("fav")) {
            if ($("#fav>li").first().text() == "Aucun favori…") {
                $("#fav").html("");
                $("#refresh").show();
                $("#saveFav").show();
            }
            $(this).parents(".center").find("button").html("<i class=\"fa fa-trash\" aria-hidden=\"true\"></i> <span class=\"hideMobile\">Supprimer ce favori</span>").click(deleteCenterParent);
            $("#fav").append($(this).parents(".center"));
            $(this).parent().addClass("fav");
            showNotificationPanel();
        }
    }

    function deleteCenterParent() {
        console.log("deleteCenterParent");
        if ($("#fav").children().length == 1) {
            $("#fav").append("<li>Aucun favori…</li>");
            $("#refresh").hide();
            $("#saveFav").hide();
        }
        $(this).parents(".center").remove();
    }

    async function getDepData(dep) {
        var result = undefined;
        try {
            result = await $.ajax({
                //url: "https://vitemadose.gitlab.io/vitemadose/" + dep + ".json"
                url: "https://dav.li/notification-vaccin/api.php?dep=" + dep
            });
        } catch (e) {
            console.error("Ajax error with dep=" + dep);
        }
        if (result && result.centres_disponibles) {
            return result.centres_disponibles.concat(result.centres_indisponibles);
        } else {
            return {};
        }
    }

    function sendNotification(center) {
        var options = {
            body: center.nom,
            tag: center.internal_id + "--" + center.prochain_rdv,
            icon: "https://dav.li/notification-vaccin/icon-192.png",
            badge: "https://dav.li/notification-vaccin/icons/medkit.png"
        };
        var notification = new Notification("Un rendez-vous est disponible !", options);
        var date = new Date();
        document.title = "(1) 🔄[" + date.getHours() + ":" + date.getMinutes() + "] – Notification vaccin Covid19";
    }

    function showNotificationPanel() {
        $("#notification").show();
        var inlineSave = generateInlineSaveFromHTML();
        console.log(inlineSave);
        $("#notification code").text("https://notification-vaccin-api.dav.li/24h/" + inlineSave + ".ics");
        $("#webcalBtn").attr("href", "webcal://notification-vaccin-api.dav.li/24h/" + inlineSave + ".ics");
    }

    $("code").click(function () {
        $(this).select();
    })

    $("#acceptCGU").click(function () {
        ipcRenderer.send('acceptedCGU', true);
        $("body>header").hide();
        $("body>main").show();
    });

    $("#showParams").click(function () {
        $("body>main").hide();
        $("#params").show();
    });

    $("#params form").submit(async function (e) {
        e.preventDefault();
        $("#params").hide();
        $("body>main").show();
        var formData = $(this).serializeArray();
        console.log(formData);
        var doctolibSyncSec = 120;
        var doctolibSyncYesNo = 'on';
        for (var f in formData) {
            if (formData[f].name == "doctolibSyncSec") {
                doctolibSyncSec = formData[f].value
            }
        }
        for (var f in formData) {
            if (formData[f].name == "doctolibSyncYesNo") {
                if (formData[f].value == "on") {
                    doctolibSyncYesNo = 'on';
                    clearInterval(realtimeDoctolibInterval);
                    doctolibRealtime();
                    realtimeDoctolibInterval = setInterval(doctolibRealtime, doctolibSyncSec * 1000);
                } else {
                    doctolibSyncYesNo = 'off';
                    clearInterval(realtimeDoctolibInterval);
                }
            }
        }
        ipcRenderer.send('saveParams', {
            'doctolibSyncSec': doctolibSyncSec,
            'doctolibSyncYesNo': doctolibSyncYesNo
        });
    });

    $("#trashData").click(function () {
        ipcRenderer.send('trashData', true);
    });
    ipcRenderer.on('trashedData', (event, args) => {
        console.log(args);
        alert("Vos données ont été totalement supprimée du stockage local. Relancez l'application pour supprimer le reste des données.")
    });

    ipcRenderer.send('pageLoaded', true);
    ipcRenderer.on('savedFav', (event, args) => {
        console.log(args);
        if (args) {
            restoreJsonSaveToHTML(args, function () {
                showNotificationPanel();
                activateNotification();
            });
        }
    });
    ipcRenderer.on('acceptedCGU', (event, args) => {
        if (args == true) {
            $("body>header").hide();
            $("body>main").show();
        }
    });
    ipcRenderer.on('params', (event, args) => {
        if (args.doctolibSyncSec) {
            $("#doctolibSyncSec").val(args.doctolibSyncSec);
        }
        if (args.doctolibSyncYesNo) {
            $("#doctolibSyncYesNo").val(args.doctolibSyncYesNo);
        }
        $("#params form").submit();
    });
});